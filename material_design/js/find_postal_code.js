$('#cod_postal').on('change', function() {
  var postal_code = $("#cod_postal").val();
                                $.ajax({
  type: "POST",
  url: "<?=base_url()?>index.php/Search_Postal_Code/postal_code/",
  data: { 'postal_code' : postal_code},
  success: function(data) {
    obj = JSON.parse(data);
    console.log(obj.data)
      var state = new Array();
      var mnpio = new Array();
      var city = new Array();
      var asenta = new Array();
      for(let i= 0; i<obj.data.datos.length; i++){
        if(!validate(obj.data.datos[i].d_estado, state)){
        state.push(obj.data.datos[i].d_estado == ""?'NO APLICA':obj.data.datos[i].d_estado);
      }
      if(!validate(obj.data.datos[i].d_ciudad, city)){
        city.push(obj.data.datos[i].d_ciudad == ""?'NO APLICA':obj.data.datos[i].d_city);
      }
      if(!validate(obj.data.datos[i].d_mnpio, mnpio)){
        mnpio.push(obj.data.datos[i].d_mnpio == ""?'NO APLICA':obj.data.datos[i].d_mnpio);
      }
      if(!validate(obj.data.datos[i].d_asenta, asenta)){
        asenta.push(obj.data.datos[i].d_asenta == ""?'NO APLICA':obj.data.datos[i].d_asenta);
      }
      }
    addOptions("d_estado", state);
    addOptions("d_ciudad", city);
    addOptions("d_municipio", mnpio);
    addOptions("d_colonia", asenta);
  },
  error: function(xhr) {
  alert("Algó salió mal!");
  }
})
});

// Rutina para agregar opciones a un <select>
function addOptions(domElement, array) {
   var select = document.getElementsByName(domElement)[0];
 for (value in array) {
  var option = document.createElement("option");
  option.text = array[value];
  option.value = value;
  select.add(option);
   $("#"+domElement).val('0');
 }
}

function validate(value, array) {
  if(value == ""){
    value = "NO APLICA";
  }
             var validate = false;
             if($.inArray(value, array)==-1){
              validate = false;
            }else{
              validate = true;
            }
            return validate;
          }

<?php

class My_bussines_model extends CI_Model
{
    private $_table;
    private $nombre_mi_empresa;
    private $rfc_mi_empresa;
    private $id_codigo_postal;
    private $correo_opcional_contacto;
    private $direccion_contacto;
    private $calle_contacto;
    private $interior_contacto;
    private $exterior_contacto;

    public function __construct()
    {
        $this->load->database();
        $this->_table = 'Contacto';
    }

    public function saveContact($contact, $bussiness)
    {
        $this->db->insert($this->_table, $contact);
        $insert_id = $this->db->insert_id();

        $bussiness['id_contacto'] = $insert_id;
        $this->_table             = 'Mi_Empresa';
        $tabla                    = $this->_table;
        $this->db->insert($this->_table, $bussiness);
        $ultimo_id = $this->db->insert_id();

        $this->db->update('Auditoria', array('status' => 0));
        $auditoria['tabla']   = $tabla;
        $auditoria['cod_reg'] = $ultimo_id;
        $auditoria['status']  = 1;
        $this->db->insert('Auditoria', $auditoria);
    }

    public function find_last_record()
    {
        $this->db->select_max('cod_reg');
        $cod_reg      = $this->db->get('Auditoria');
        $my_bussiness = $this->db->get_where('Mi_Empresa', array('id_mi_empresa' =>
            $cod_reg->row_array()['cod_reg']));
        $my_contact = $this->db->get_where('Contacto', array('id_contacto' =>
            $my_bussiness->row_array()['id_contacto']));
        $postal_code = $this->db->get_where('Codigo_Postal', array('id_codigo_postal' =>
            $my_contact->row_array()['id_codigo_postal']));

        $contact                                = array();
        $contact['id_codigo_postal']            = $my_contact->row_array()['id_codigo_postal'];
        $contact['d_codigo']                    = $postal_code->row_array()['d_codigo'];
        $contact['d_asenta']                    = $postal_code->row_array()['d_asenta'];
        $contact['d_mnpio']                     = $postal_code->row_array()['d_mnpio'];
        $contact['d_estado']                    = $postal_code->row_array()['d_estado'];
        $contact['d_ciudad']                    = $postal_code->row_array()['d_ciudad'];
        $contact['telefono_principal_contacto'] = $my_contact->row_array()['telefono_principal_contacto'];
        $contact['correo_opcional_contacto']    = $my_contact->row_array()['correo_opcional_contacto'];
        $contact['direccion_contacto']          = $my_contact->row_array()['direccion_contacto'];
        $contact['calle_contacto']              = $my_contact->row_array()['calle_contacto'];
        $contact['exterior_contacto']           = $my_contact->row_array()['exterior_contacto'];
        $contact['interior_contacto']           = $my_contact->row_array()['interior_contacto'];
        $contact['nombre_mi_empresa']           = $my_bussiness->row_array()['nombre_mi_empresa'];
        $contact['rfc_mi_empresa']              = $my_bussiness->row_array()['rfc_mi_empresa'];
        return $contact;
        /*$query = $this->db->get_where('Mi_Empresa', array('id_mi_empresa' => $code));
    return $query->result();*/
    }
}

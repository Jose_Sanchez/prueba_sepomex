<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Upload_File extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('postal_code_model');

    }

    public function index()
    {
        $this->load->view("cpanel/inc/header.php");
        $this->load->view("cpanel/inc/menu.php");
        $this->load->view("cpanel/sepomex/upload_file.php");
        $this->load->view("cpanel/inc/footer.php");
    }

    public function save()
    {
        set_time_limit(1200);

        $config['upload_path']   = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size']      = "80000K";
        $config['max_width']     = "1024";
        $config['max_height']    = "768";

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            echo "Ha ocurrido un error";
        } else {
            $data      = array('upload_data' => $this->upload->data());
            $separator = trim($this->input->post('separator'));
            $line      = (int) trim($this->input->post('line'));
            $file      = fopen("./" . "uploads/" . $data['upload_data']['file_name'], 'r');
            $i         = 0;

            while ($line = fgetcsv($file, 0, $separator)) {
                $postal_code = array();
                if ($i >= 3) {
                    $postal_code['d_codigo']         = $line[0];
                    $postal_code['d_asenta']         = $line[1];
                    $postal_code['d_tipo_asenta']    = $line[2];
                    $postal_code['D_mnpio']          = $line[3];
                    $postal_code['d_estado']         = $line[4];
                    $postal_code['d_ciudad']         = $line[5];
                    $postal_code['d_CP']             = $line[6];
                    $postal_code['c_estado']         = $line[7];
                    $postal_code['c_oficina']        = $line[8];
                    $postal_code['c_CP']             = $line[9];
                    $postal_code['c_tipo_asenta']    = $line[10];
                    $postal_code['c_mnpio']          = $line[11];
                    $postal_code['id_asenta_cpcons'] = $line[12];
                    $postal_code['d_zona']           = $line[13];
                    $postal_code['c_cve_ciudad']     = $line[14];
                    $this->postal_code_model->saveCode($postal_code);
                }
                $i++;
            }
        }

    }
}

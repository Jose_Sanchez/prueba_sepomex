<?php
defined('BASEPATH') or exit('No direct script access allowed');

class My_Bussines extends CI_Controller
{

    private $_table;
    private $nombre_mi_empresa;
    private $rfc_mi_empresa;
    private $id_codigo_postal;
    private $correo_opcional_contacto;
    private $direccion_contacto;
    private $calle_contacto;
    private $interior_contacto;
    private $exterior_contacto;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('postal_code_model');
        $this->load->model('my_bussines_model');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->load->view("cpanel/inc/header.php");
        $this->load->view("cpanel/inc/menu.php");
        $this->load->view("cpanel/sepomex/my_bussines.php");
        $this->load->view("cpanel/inc/footer.php");
    }

    public function find_postal_code()
    {
        $codigo = $this->input->post();
        $cod    = $codigo['postal_code'];

        $codes = $this->postal_code_model->findCode($cod);

        $data          = new stdClass();
        $data->message = "Postal Code Found";
        $data->datos   = $codes;

        $response          = new stdClass();
        $response->success = true;
        $response->data    = $data;

        echo json_encode($response);
    }

    public function save_bussiness()
    {
        $data = $this->input->post();

        $contact                                = array();
        $contact['id_codigo_postal']            = $data['postal_code'];
        $contact['telefono_principal_contacto'] = $data['tlf'];
        $contact['correo_opcional_contacto']    = $data['email'];
        $contact['direccion_contacto']          = $data['domic'];
        $contact['calle_contacto']              = $data['stream'];
        $contact['exterior_contacto']           = $data['next'];
        $contact['interior_contacto']           = $data['nint'];

        $bussiness                      = array();
        $bussiness['nombre_mi_empresa'] = $data['name'];
        $bussiness['rfc_mi_empresa']    = $data['rfc'];

        $this->my_bussines_model->saveContact($contact, $bussiness);
    }

    public function find_last_record()
    {
        $data          = new stdClass();
        $data->message = "Postal Code Found";
        $data->datos   = $this->my_bussines_model->find_last_record();
        echo json_encode($data);
    }

}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Search_Postal_Code extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('postal_code_model');

    }

    public function index()
    {
        $this->load->view("cpanel/inc/header.php");
        $this->load->view("cpanel/inc/menu.php");
        $this->load->view("cpanel/sepomex/search_postal_code.php");
        $this->load->view("cpanel/inc/footer.php");
    }

    public function postal_code()
    {
        $codigo = $this->input->post();
        $cod    = $codigo['postal_code'];

        $codes = $this->postal_code_model->findCode($cod);

        $data          = new stdClass();
        $data->message = "Postal Code Found";
        $data->datos   = $codes;

        $response          = new stdClass();
        $response->success = true;
        $response->data    = $data;

        echo json_encode($response);
    }

}

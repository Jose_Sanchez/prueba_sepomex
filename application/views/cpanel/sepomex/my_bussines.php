<section class="content">
    <div class="container-fluid">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Mi Empresa
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button">
                                <i class="material-icons">
                                    more_vert
                                </i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a class=" waves-effect waves-block" href="javascript:void(0);">
                                        Action
                                    </a>
                                </li>
                                <li>
                                    <a class=" waves-effect waves-block" href="javascript:void(0);">
                                        Another action
                                    </a>
                                </li>
                                <li>
                                    <a class=" waves-effect waves-block" href="javascript:void(0);">
                                        Something else here
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Nombre de Empresa*
                                </label>
                                <div class="form-line">
                                    <input class="form-control" id="ne" name="ne" type="text">
                                    </input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    RFC*
                                </label>
                                <div class="form-line">
                                    <input class="form-control" id="nr" name="nr" type="text">
                                    </input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Teléfono*
                                </label>
                                <div class="form-line">
                                    <input class="form-control" id="nt" name="nt" type="text">
                                    </input>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Correo Electrónico*
                                </label>
                                <div class="form-line">
                                    <input class="form-control"
                                    id="nc" name="nc" type="text">
                                    </input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Domicilio
                                </label>
                                <div class="form-line">
                                    <input class="form-control" id="nd" name="nd" type="text">
                                    </input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Calle
                                </label>
                                <div class="form-line">
                                    <input class="form-control" id="nca" name="nca" type="text">
                                    </input>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Número Exterior
                                </label>
                                <div class="form-line">
                                    <input class="form-control" id="nex" name="nex" type="text">
                                    </input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Número Interior
                                </label>
                                <div class="form-line">
                                    <input class="form-control" id="ni" name="ni" type="text">
                                    </input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Código Postal*
                                </label>
                                <div class="form-line">
                                    <input class="form-control col-md-7 col-xs-12" id="cod_postal" name="cod_postal" placeholder="p. ej. 01070" type="text">
                                    </input>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Estado*
                                </label>
                                <div class="form-line">
                                    <select class="form-control col-md-7 col-xs-12" id="d_estado" name="d_estado" style="text-transform: uppercase;">
                                        <option selected="selected">
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Ciudad*
                                </label>
                                <div class="form-line">
                                    <select class="form-control col-md-7 col-xs-12" id="d_ciudad" name="d_ciudad" style="text-transform: uppercase;">
                                        <option>
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Municipio*
                                </label>
                                <div class="form-line">
                                    <select class="form-control col-md-7 col-xs-12" id="d_municipio" name="d_municipio" style="text-transform: uppercase;">
                                        <option selected="selected">
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Colonia*
                                </label>
                                <div class="form-line">
                                    <select class="form-control col-md-7 col-xs-12" id="d_colonia" name="d_colonia" style="text-transform: uppercase;">
                                        <option selected="selected">
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                            <button class="btn btn-success m-t-15 waves-effect" id="update" name="update">
                                Actualizar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
     $('#cod_postal').on('change', function() {
  var postal_code = $("#cod_postal").val();
                                $.ajax({
  type: "POST",
  url: "<?=base_url()?>index.php/My_Bussines/find_postal_code/",
  data: { 'postal_code' : postal_code},
  success: function(data) {
    obj = JSON.parse(data);
    console.log(obj.data)
      var state = new Array();
      var mnpio = new Array();
      var city = new Array();
      var asenta = new Array();
      for(let i= 0; i<obj.data.datos.length; i++){
        if(!validate(obj.data.datos[i].d_estado, state)){
        state.push(obj.data.datos[i].d_estado == ""?'NO APLICA':obj.data.datos[i].d_estado);
      }
      if(!validate(obj.data.datos[i].d_ciudad, city)){
        city.push(obj.data.datos[i].d_ciudad == ""?'NO APLICA':obj.data.datos[i].d_city);
      }
      if(!validate(obj.data.datos[i].d_mnpio, mnpio)){
        mnpio.push(obj.data.datos[i].d_mnpio == ""?'NO APLICA':obj.data.datos[i].d_mnpio);
      }
      if(!validate(obj.data.datos[i].d_asenta, asenta)){
        asenta.push(obj.data.datos[i].d_asenta == ""?'NO APLICA':obj.data.datos[i].d_asenta);
      }
      }
    addOptions("d_estado", state);
    addOptions("d_ciudad", city);
    addOptions("d_municipio", mnpio);
    addOptions("d_colonia", asenta);
  },
  error: function(xhr) {
  alert("Algó salió mal!");
  }
})
});

// Rutina para agregar opciones a un <select>
function addOptions(domElement, array) {
   var select = document.getElementsByName(domElement)[0];
 for (value in array) {
  var option = document.createElement("option");
  option.text = array[value];
  option.value = value;
  select.add(option);
 }
}

function validate(value, array) {
  if(value == ""){
    value = "NO APLICA";
  }
             var validate = false;
             if($.inArray(value, array)==-1){
              validate = false;
            }else{
              validate = true;
            }
            return validate;
          }

          $('#update').click(function() {
          	if($("#ne").val() == "" ||
          		$("#nr").val() == "" ||
          		$("#nt").val() == "" ||
          		$("#nc").val() == "" ||
          		$("#cod_postal").val() == ""){
          		alert("Debe llenar los campos obligatorios");
          	}else{
          		                           $.ajax({
  type: "POST",
  url: "<?=base_url()?>index.php/My_Bussines/save_bussiness/",
  data: { 'postal_code' : $("#cod_postal").val(),
  'name' : $("#ne").val(),
  'rfc' : $("#nr").val(),
  'tlf' : $("#nt").val(),
  'email' : $("#nc").val(),
  'domic' : $("#nd").val(),
  'stream' : $("#nca").val(),
  'next' : $("#nex").val(),
  'nint' : $("#ni").val()
},
  success: function(data) {
    alert("Se ha guardado con éxito");
    window.location.href = "<?php echo site_url('index.php'); ?>";
},
error: function(xhr){
	alert("Algo salió mal!")
}
          	});
          		                       }
          });

          window.onload=function() {
			$.ajax({
  type: "POST",
  url: "<?=base_url()?>index.php/My_Bussines/find_last_record/",
  data: {'hola': 'gola'},
  success: function(data) {
    obj = JSON.parse(data);
    console.log(obj.datos)
  $("#cod_postal").val(obj.datos["d_codigo"]);
  $("#ne").val(obj.datos["nombre_mi_empresa"]);
  $("#nr").val(obj.datos["rfc_mi_empresa"]);
  $("#nt").val(obj.datos["telefono_principal_contacto"]);
  $("#nc").val(obj.datos["correo_opcional_contacto"]);
  $("#nd").val(obj.datos["d_codigo"]);
  $("#nca").val(obj.datos["calle_contacto"]);
  $("#nex").val(obj.datos["exterior_contacto"]);
  $("#ni").val(obj.datos["interior_contacto"]);
  $("#d_estado option:selected").html(obj.datos["d_estado"] == "" ?"NO_APLICA" : obj.datos["d_estado"]);
  $("#d_municipio option:selected").html(obj.datos["d_mnpio"] == "" ?"NO_APLICA" : obj.datos["d_mnpio"]);
  $("#d_ciudad option:selected").html(obj.datos["d_ciudad"] == "" ?"NO_APLICA" : obj.datos["d_ciudad"]);
  $("#d_colonia option:selected").html(obj.datos["d_asenta"] == "" ?"NO_APLICA" : obj.datos["d_asenta"]);
},
error: function(xhr){
	alert("Algo salió mal!")
}
          	});
		}
</script>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <center>
                <h1>
                    Ejemplo Sepomex
                </h1>
            </center>
            <br>
                <div class="clearfix">
                </div>
                <center>
                    <h4>
                        Ej: 01070, 97345, 06600
                    </h4>
                </center>
                <br>
                    <br>
                        <div class="clearfix">
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cod_postal">
                                        Código Postal
                                        <span class="required">
                                            *
                                        </span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input class="form-control col-md-7 col-xs-12" 
                                        id="cod_postal" name="cod_postal" placeholder="p. ej. 01070" type="text">
                                        </input>
                                    </div>
                                </div>
                                <br>
                                    <div class="clearfix">
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="d_estado">
                                            Estado
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control col-md-7 col-xs-12" id="d_estado" name="d_estado" style="text-transform: uppercase;">
                                                <option selected="selected">
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                        <div class="clearfix">
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="d_ciudad">
                                                Ciudad
                                                <span class="required">
                                                    *
                                                </span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="form-control col-md-7 col-xs-12" id="d_ciudad" name="d_ciudad" style="text-transform: uppercase;">
                                                    <option>
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                            <div class="clearfix">
                                            </div>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="d_municipio">
                                                    Municipio
                                                    <span class="required">
                                                        *
                                                    </span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select class="form-control col-md-7 col-xs-12" id="d_municipio" name="d_municipio" style="text-transform: uppercase;">
                                                        <option selected="selected">
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                                <div class="clearfix">
                                                </div>
                                                <div class="item form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="d_colonia">
                                                        Colonia
                                                        <span class="required">
                                                            *
                                                        </span>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select class="form-control col-md-7 col-xs-12" id="d_colonia" name="d_colonia" style="text-transform: uppercase;">
                                                            <option selected="selected">
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </br>
                                        </br>
                                    </br>
                                </br>
                            </div>
                            <br>
                                <div class="clearfix">
                                </div>
                            </br>
                        </div>
                    </br>
                </br>
            </br>
        </div>
    </div>
</section>
<script type="text/javascript">
     $('#cod_postal').on('change', function() {
  var postal_code = $("#cod_postal").val();
                                $.ajax({
  type: "POST",
  url: "<?=base_url()?>index.php/Search_Postal_Code/postal_code/",
  data: { 'postal_code' : postal_code},
  success: function(data) {
    obj = JSON.parse(data);
    console.log(obj.data)
      var state = new Array();
      var mnpio = new Array();
      var city = new Array();
      var asenta = new Array();
      for(let i= 0; i<obj.data.datos.length; i++){
        if(!validate(obj.data.datos[i].d_estado, state)){
        state.push(obj.data.datos[i].d_estado == ""?'NO APLICA':obj.data.datos[i].d_estado);
      }
      if(!validate(obj.data.datos[i].d_ciudad, city)){
        city.push(obj.data.datos[i].d_ciudad == ""?'NO APLICA':obj.data.datos[i].d_city);
      }
      if(!validate(obj.data.datos[i].d_mnpio, mnpio)){
        mnpio.push(obj.data.datos[i].d_mnpio == ""?'NO APLICA':obj.data.datos[i].d_mnpio);
      }
      if(!validate(obj.data.datos[i].d_asenta, asenta)){
        asenta.push(obj.data.datos[i].d_asenta == ""?'NO APLICA':obj.data.datos[i].d_asenta);
      }
      }
    addOptions("d_estado", state);
    addOptions("d_ciudad", city);
    addOptions("d_municipio", mnpio);
    addOptions("d_colonia", asenta);
  },
  error: function(xhr) {
  alert("Algó salió mal!");
  }
})
});

// Rutina para agregar opciones a un <select>
function addOptions(domElement, array) {
   var select = document.getElementsByName(domElement)[0];
 for (value in array) {
  var option = document.createElement("option");
  option.text = array[value];
  option.value = value;
  select.add(option);
   $("#"+domElement).val('0');
 }
}

function validate(value, array) {
  if(value == ""){
    value = "NO APLICA";
  }
             var validate = false;
             if($.inArray(value, array)==-1){
              validate = false;
            }else{
              validate = true;
            }
            return validate;
          }
</script>

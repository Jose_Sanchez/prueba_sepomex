<section>
    <aside class="sidebar" id="leftsidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img alt="User" height="48" src="<?=base_url()?>/material_design/images/user.png" width="48">
                </img>
            </div>
            <div class="info-container">
                <div aria-expanded="false" aria-haspopup="true" class="name" data-toggle="dropdown">
                    John Doe
                </div>
                <div class="email">
                    john.doe@example.com
                </div>
                <div class="btn-group user-helper-dropdown">
                    <i aria-expanded="true" aria-haspopup="true" class="material-icons" data-toggle="dropdown">
                        keyboard_arrow_down
                    </i>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a class=" waves-effect waves-block" href="javascript:void(0);">
                                <i class="material-icons">
                                    person
                                </i>
                                Profile
                            </a>
                        </li>
                        <li class="divider" role="seperator">
                        </li>
                        <li>
                            <a class=" waves-effect waves-block" href="javascript:void(0);">
                                <i class="material-icons">
                                    group
                                </i>
                                Followers
                            </a>
                        </li>
                        <li>
                            <a class=" waves-effect waves-block" href="javascript:void(0);">
                                <i class="material-icons">
                                    shopping_cart
                                </i>
                                Sales
                            </a>
                        </li>
                        <li>
                            <a class=" waves-effect waves-block" href="javascript:void(0);">
                                <i class="material-icons">
                                    favorite
                                </i>
                                Likes
                            </a>
                        </li>
                        <li class="divider" role="seperator">
                        </li>
                        <li>
                            <a class=" waves-effect waves-block" href="javascript:void(0);">
                                <i class="material-icons">
                                    input
                                </i>
                                Sign Out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 376px;">
                <ul class="list" style="overflow: hidden; width: auto; height: 376px;">
                    <li class="header">
                        MAIN NAVIGATION
                    </li>
                    <li class="active">
                        <a class="toggled waves-effect waves-block" href="<?php echo site_url('index.php/Upload_File'); ?>">
                            <i class="material-icons">
                                file_upload
                            </i>
                            <span>
                                Cargar Archivo
                            </span>
                        </a>
                    </li>
                    <li>
                        <a class=" waves-effect waves-block" href="<?php echo site_url('index.php/Search_Postal_Code'); ?>">
                            <i class="material-icons">
                                find_in_page
                            </i>
                            <span>
                                Buscar Código Postal
                            </span>
                        </a>
                    </li>
                         <a class=" waves-effect waves-block" href="<?php echo site_url('index.php/My_Bussines'); ?>">
                            <i class="material-icons">
                                business_center
                            </i>
                            <span>
                                Mi Empresa
                            </span>
                        </a>
                    <div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.5); width: 4px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 152.017px;">
                    </div>
                    <div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;">
                    </div>
                </ul>
            </div>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                © 2016 - 2017
                <a href="javascript:void(0);">
                    AdminBSB - Material Design
                </a>
                .
            </div>
            <div class="version">
                <b>
                    Version:
                </b>
                1.0.5
            </div>
        </div>
        <!-- #Footer -->
    </aside>
</section>

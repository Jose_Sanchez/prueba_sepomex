<?php $this->load->view('cpanel/inc/header');?>
<?php $this->load->view('cpanel/inc/menu');?>
<section class="content">
	<div id="container">
		<div id="body">
        <div class="block-header">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                BIENVENIDO A LA PRUEBA SEPOMEX
                            </h2>
                        </div>
                        <div class="body">
                        	<div id="frmFileUpload" class="dropzone" type="file">
                                <div class="dz-message">
                                    <div class="drag-icon-cph">
                                        <i class="material-icons">mail_outline</i>
                                    </div>
                                     <p style="color: blue;text-align: center;font-size: 30px">
                               Seleccione una opción del menú para comenzar la prueba
                            </p>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
		</div>
	</div>
	</section>
<?php $this->load->view('cpanel/inc/footer');?>